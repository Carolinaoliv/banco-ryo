package com.folcademy.banco_ryo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoRyoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoRyoApplication.class, args);
	}

}
